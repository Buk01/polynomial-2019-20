/** Integer-only polynomials. */
public class Polynomial {

	/**
	 * Create new instance with given coefficients.
	 *
	 * x^2 + 3x - 7 would be created by new Polynomial(-7, 3, 1) x^5 + 42 would be
	 * created by new Polynomial(42, 0, 0, 0, 0, 1)
	 *
	 * @param coef Coefficients, ordered from lowest degree (power).
	 */
	private int[] coefficients;

	public Polynomial(int... coef) {
		System.out.printf("Creating polynomial");
		for (int i = 0; i < coef.length; i++) {
			if (i > 0) {
				System.out.print(" +");
			}
			System.out.printf(" %d * x^%d", coef[i], i);
		}
		System.out.println(" ...");
		coefficients = coef;
	}

	/** Get coefficient value for given degree (power). */
	public int getCoefficient(int deg) {
		if (deg > (coefficients.length - 1) || deg < 0) {
			return 0;
		}
		return coefficients[deg];
	}

	/** Get degree (max used power) of the polynomial. */
	public int getDegree() {
		if (coefficients.length == 1) {
			return 1;
		}
		if (coefficients.length <= 0) {
			return 0;
		}
		for (int i = coefficients.length - 1; i >= 0; i--) {
			if (coefficients[i] != 0) {
				return i;
			}
		}
		return coefficients.length - 1;
	}

	/** Format into human-readable form with given variable. */
	public String toPrettyString(String variable) {
		String prettyString = "";
		boolean first = true;
		for (int i = coefficients.length - 1; i >= 0; i--) {
			int coef = coefficients[i];
			if (coef != 0) {
				if (first) {
					if (coef < 0) {
						prettyString += "-";
					}
				} else {
					if (coef > 0) {
						prettyString += " + ";
					} else {
						prettyString += " - ";
					}
				}
				first = false;
				if (coef != 1 || i == 0) {
					prettyString += Math.abs(coef);
				}

				if (i >= 1) {
					prettyString += variable;
				}
				if (i > 1) {
					prettyString += "^" + i;
				}
			}
		}
		return prettyString;
	}

	/** Debugging output, dump only coefficients. */
	@Override
	public String toString() {
		String output = "";
		for (int i = 0; i < coefficients.length; i++) {
			output += coefficients[i];
			if (i < coefficients.length - 1) {
				output += ",";
			}
		}
		return "Polynomial[" + output + "]";
	}

	/** Adds together given polynomials, returning new one. */
	public static Polynomial sum(Polynomial... polynomials) {
		int[] sum = new int[polynomials[1].getDegree() + 1];
		if (polynomials[0].getDegree() > polynomials[1].getDegree()) {
			sum = new int[polynomials[0].getDegree() + 1];
		}
		
		for (int i = 0; i < sum.length; i++) {
			sum[i] = polynomials[0].getCoefficient(i) + polynomials[1].getCoefficient(i);
		}
		return new Polynomial(sum);
	}

	/** Multiplies together given polynomials, returning new one. */
	public static Polynomial product(Polynomial... polynomials) {
		int[] product = new int[polynomials[0].getDegree() + polynomials[1].getDegree() + 1];
		for (int i = 0; i <= polynomials[0].getDegree(); i++) {
			for (int j = 0; j <= polynomials[1].getDegree(); j++) {
				product[i + j] += polynomials[0].getCoefficient(i) * polynomials[1].getCoefficient(j);
			}
		}
		return new Polynomial(product);
	}

	/** Get the result of division of two polynomials, ignoring remainder. */
	public static Polynomial div(Polynomial dividend, Polynomial divisor) {
		int[] div = new int[dividend.getDegree() - divisor.getDegree() + 1];
		Polynomial temporaryDividend = dividend;
		while (divisor.getDegree() <= temporaryDividend.getDegree()) {
			if (temporaryDividend.degreeValue() % divisor.degreeValue() != 0) {
				throw new ArithmeticException("Division result will not be a whole number.");
			}
			int currentCoef = temporaryDividend.degreeValue() / divisor.degreeValue();
			int currentDegree = temporaryDividend.getDegree() - divisor.getDegree();
			div[currentDegree] = currentCoef;
			int[]currentDivCoef = new int[currentDegree + 1];
			currentDivCoef [currentDegree] = currentCoef;
			Polynomial currentDiv = new Polynomial(currentDivCoef);
			Polynomial temporaryResult = product(currentDiv, divisor);
			temporaryDividend = sub(temporaryDividend, temporaryResult);
		}
		return new Polynomial(div);
	}

	/** Get the remainder of division of two polynomials. */
	public static Polynomial mod(Polynomial dividend, Polynomial divisor) {
		return sub(dividend, product(divisor, div(dividend, divisor)));
	}
	
	/** Subtracts second polynomial from the first. */
	public static Polynomial sub(Polynomial a, Polynomial b) {
		return sum(a, b.negative());
	}

	public Polynomial negative() {
		int[] negativeCoef = new int[coefficients.length];
		for (int i = 0; i < coefficients.length; i++) {
			negativeCoef[i] = - coefficients[i];
		}
		return new Polynomial(negativeCoef);
	}
	/** Expands the polynomial by the given integer */
	public Polynomial expand(int expandBy) {
		int[] expanded = new int[coefficients.length];
		for (int i = 0; i < coefficients.length; i++) {
			expanded[i] = coefficients[i] * expandBy;
		}
		return new Polynomial(expanded);
	}
	private int degreeValue() {
		return getCoefficient(getDegree());
	}
}
		
