
public class Main {
	public static void main(String[] args) {
		if (args.length < 4) {
			printHelp();
			return;
		}

		String op = args[0];

		int delimPosition = 0;
		for (int i = 1; i < args.length; i++) {
			if (args[i].equals("--")) {
				delimPosition = i;
				break;
			}
		}
		if (delimPosition == 0) {
			printHelp();
			return;
		}

		int[] coefFirst = parseCoefficients(args, 1, delimPosition);
		int[] coefSecond = parseCoefficients(args, delimPosition + 1, args.length);

		if ((coefFirst.length == 0) || (coefSecond.length == 0)) {
			printHelp();
			return;
		}

		Polynomial first = new Polynomial(coefFirst);
		Polynomial second = new Polynomial(coefSecond);

		Polynomial result;
		Polynomial remainder = null;
		String opSymbol;
		if (op.equals("plus")) {
			result = Polynomial.sum(first, second);
			opSymbol = "+";
		} else if (op.equals("times")) {
			result = Polynomial.product(first, second);
			opSymbol = "*";
		} else if (op.equals("divided")) {
			result = Polynomial.div(first, second);
			remainder = Polynomial.mod(first, second);
			opSymbol = "/";
		} else {
			printHelp();
			return;
		}
		
		System.out.printf("(%s) %s (%s) = (%s) %s\n", first.toPrettyString("x"), opSymbol, second.toPrettyString("x"),
				result.toPrettyString("x"), remainder != null ? ", remainder: (" + remainder.toPrettyString("x") + ")": "");
	}

	private static void printHelp() {
		System.out.println("Usage:");
		System.out.println("The first argument is the operator between the two polynomials:");
		System.out.printf(" - \"plus\" is the addition operator (+)\n - \"times\" is the multiplication operator (*)\n - \"divided\" is the division operator (/); make sure that the result of the division will be a whole number\n");
		System.out.println("The second set of arguments are coefficients of the first polynomial, powers descending. For example: 1 3 -7 creates x^2 + 3x - 7");
		System.out.println("The two polynomials are divided by \"--\"");
		System.out.println("The last set of arguments are coefficients of the second polynomial, powers descending.");
		System.out.println("Example input: plus 1 3 -7 -- 4 5 0 10");
	}

	private static int[] parseCoefficients(String[] args, int firstIndexIncl, int lastIndexExcl) {
		int[] result = new int[lastIndexExcl - firstIndexIncl];
		int resIndex = result.length - 1;
		for (int i = firstIndexIncl; i < lastIndexExcl; i++) {
			result[resIndex] = Integer.parseInt(args[i]);
			resIndex--;
		}
		return result;
	}

}
